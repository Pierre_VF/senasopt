import numpy as np
import pandas as pd
import pytest

from senasopt.asset.battery import Battery


def test_battery_opt():
    capacity = 10.0
    rated_power = 5.0
    __t = pd.date_range("2021-01-01", "2021-01-05", freq="1H")
    __price_t = __t.hour
    df_price = pd.DataFrame(
        {"local_demand": 0, "local_production": 0, "power_price": __price_t}, index=__t
    )

    battery = Battery(capacity=capacity, rated_power=rated_power)
    battery.set_state(soc=0)
    performance, df_opt_out = battery.optimal_schedule(df_price)
    assert isinstance(df_opt_out, pd.DataFrame)

    df_price2 = pd.DataFrame(
        {
            "local_demand": 0,
            "local_production": 0,
            "power_price_import": __price_t - 1,
            "power_price_export": __price_t,  # TODO : looks like opt does not converge if different
        },
        index=__t,
    )
    battery2 = Battery(capacity=capacity, rated_power=rated_power, connection_sizing=2)
    battery2.set_state(soc=0)
    performance2, df_opt_out2 = battery2.optimal_schedule(df_price2)
    assert isinstance(df_opt_out2, pd.DataFrame)

    with pytest.raises(Exception):
        battery3 = Battery(
            capacity=capacity, rated_power=rated_power, connection_sizing=np.inf
        )
        battery3.set_state(soc=0)
        performance3, df_opt_out3 = battery3.optimal_schedule(df_price2)
